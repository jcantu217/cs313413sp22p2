
TestList.java 

Questions: 
list.remove(5); // what does this method do?
** This method removes an item at that specific index.

list.remove(Integer.valueOf(5)); // what does this one do?
** This method removes an integer value equal to the number inputed, in this case 5. 


TestIterator.java

i.remove(); // TODO what happens if you use list.remove(Integer.valueOf(77))?
** list.remove(Integer.valueOf(77)) will remove only a single instance of the integer 
value 77. We need to use the iterator to sort through the list and remove it for every single instance. 


Test Performance:

Remove
-Linked List: size = 10 (142 ms), 100 (77 ms), 1000 (158 ms), 10000 (223 ms)
-Array: for size = 10 (166 ms), 100 (213 ms), 1000 (534 ms), 10000 (679 ms)

**When it comes to removing items, linked lists performs better as the size increases. This is because linked lists do not have to expand [memory allocation] or resize when inserting or deleting items from the list.

** The worst time complexity to remove an item in an array is O(n) while for a linked list is O(1). Linked lists perform better when deleting and inserting items compared to arrays. 

Access 
-Linked List: size = 10 (97 ms), 100 (143 ms), 1000 (646 ms), 10000 (10 sec, 387 ms)
-Array: for size = 10 (51 ms), 100 (52 ms), 1000 (37 ms), 10000 (57 ms)

**When it comes to accessing items, array lists perform better as the size increases. This is because we don't have to traverse the whole list to access a specific item, instead we can quickly access it based on its index. 

** The worst time complexity to acces an item in an array is O(1) while for a linked list is O(n). 



Notes from Stack Overflow: 

Linked lists are preferable over arrays when:

a) you need constant-time insertions/deletions from the list (such as in real-time computing where time predictability is absolutely critical)

b) you don't know how many items will be in the list. With arrays, you may need to re-declare and copy memory if the array grows too big

c) you don't need random access to any elements

d) you want to be able to insert items in the middle of the list (such as a priority queue)

Arrays are preferable when:

a) you need indexed/random access to elements

b) you know the number of elements in the array ahead of time so that you can allocate the correct amount of memory for the array

c) you need speed when iterating through all the elements in sequence. You can use pointer math on the array to access each element, whereas you need to lookup the node based on the pointer for each element in linked list, which may result in page faults which may result in performance hits.

d) memory is a concern. Filled arrays take up less memory than linked lists. Each element in the array is just the data. Each linked list node requires the data as well as one (or more) pointers to the other elements in the linked list.

Array Lists (like those in .Net) give you the benefits of arrays, but dynamically allocate resources for you so that you don't need to worry too much about list size and you can delete items at any index without any effort or re-shuffling elements around. Performance-wise, arraylists are slower than raw arrays.

Reference: Lamar answer https://stackoverflow.com/a/393578/6249148


